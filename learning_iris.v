(* Contains definitions of the weakest precondition
   assertion, and its basic rules. *)
From iris.program_logic Require Export weakestpre.

(* Definition of invariants and their rules (expressed
   using the fancy update modality). *)
From iris.base_logic.lib Require Export invariants.

(* Files related to the interactive proof mode. The
   first import includes the general tactics of the
   proof mode. The second provides some more specialized
   tactics particular to the instantiation of Iris to a
   particular programming language. *)
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.

(* Instantiation of Iris with the particular language.
   The notation file contains many shorthand notations
   for the programming language constructs, and the
   lang file contains the actual language syntax. *)
From iris.heap_lang Require Export notation lang.

(* We also import the parallel composition construct.
   This is not a primitive of the language, but is
   instead derived. This file contains its definition,
   and the proof rule associated with it. *)
From iris.heap_lang.lib Require Import par.

(* The following line imports some Coq configuration
   we commonly use in Iris projects, mostly with the
   goal of catching common mistakes. *)
From iris.prelude Require Import options.

Section learn_bi.
Variable PROP : bi.

Print bi.
About "∗".

Print PROP.

Print Scope bi_scope.

Print bi_emp_valid.
Check bi_entails.

Lemma test (a b c : PROP) : (a ∗ b) -∗ (b ∗ a).
Proof.
	iIntros "(Ha&Hb)".
	iSplitL "Hb"; iAssumption.
Qed.

Goal forall (P Q : PROP), □ P ∗ Q ⊢ (P ∗ Q) ∗ P.
Proof.
	iIntros (P Q) "(#HpP & HQ)".
	iSplitL "HQ".
	{ iSplitR "HQ"; iAssumption. }
	iAssumption.
Qed.

End learn_bi.

Section learn_heap.

Context `{!heapGS Σ}.
Implicit Types l : loc.

Notation iProp := (iProp Σ).

Check #().
Check Val #().

Goal {{{ True }}} Val #() {{{RET #(); True }}}.
Proof.
	iIntros (Ψ) "_ H".
	wp_pures.
	iModIntro.
Abort. (* Why? *)
(* Because texan triples only work on programs that
   execute at least 1 step (see next example). *)

Definition loop : val :=
	rec: "loop" "x" := "loop" "x".

Goal {{{ True }}} loop #() {{{ RET #(); False }}}.
Proof.
	iIntros (Φ) "_ _".
	iLöb as "IH".
	wp_rec.
	done.
Qed.

(* To reason about value programs, use straight WP *)
(* Need of_val because of
	https://gitlab.mpi-sws.org/iris/iris/-/merge_requests/656 *)
Goal ⊢ WP of_val #() {{ v, True }}.
Proof. by wp_pures. Qed.

Goal {{{ True }}} #() ;; #() {{{RET #(); True }}}.
Proof.
	iIntros (Ψ) "_ H".
	wp_pures.
	iModIntro.
	iApply "H".
	done.
Qed.

(* Change to val and make ℓ an argument in heap lang *)
Definition incr (ℓ : loc) : expr := #ℓ <- !#ℓ + #1.
Print incr.

Lemma incr_spec ℓ (n : Z) :
	{{{ ℓ ↦ #n }}}
		incr ℓ ;; !#ℓ
	{{{m, RET #m; ⌜m = (n + 1)%Z⌝ }}}.
Proof.
	iIntros (Ψ) "ℓn H".
	unfold incr.
	wp_bind (!_)%E.
	wp_load.
		(* Why does this affect the ▷ in H ? *)
		(* Intuitively, because if ▷P holds now, then P hold later (after 1 step) *)
	wp_store.
	wp_load.
	iModIntro.
	by iApply ("H" $! (n+1)%Z).
Qed.

Lemma incr_spec_2 ℓ (n : Z) :
	{{{ ℓ ↦ #n }}}
		incr ℓ
	{{{RET #(); (ℓ ↦ #(n + 1)%Z) }}}.
Proof.
	iIntros (Ψ) "ℓn H".
	unfold incr.
	wp_load.
	wp_store.
	iModIntro.
	iApply ("H" with "ℓn").
Qed.

(* Why should this be of type val and not expr? *)
(* Because vals dont get unfolded by subst (among other things) *)
(* Most things should be val *)
Definition nondet_bool : val :=
  λ: <>, let: "l" := ref #true in
  	Fork ("l" <- #false);; !"l".

Lemma nondet_bool_spec :
	{{{ True }}}
		nondet_bool #()
	{{{ (b : bool), RET #b; True }}}.
Proof.
  iIntros (Φ) "_ HΦ".
  unfold nondet_bool.
	wp_pures.
  wp_alloc l as "Hl".
  wp_pures.
  pose proof (nroot .@ "rnd") as rndN.
  iMod
  	(inv_alloc rndN _ (∃ (b : bool), l ↦ #b)%I with "[Hl]")
  	as "#Hinv".
  { iNext. iExists true; iAssumption. }
(*   iMod "Hinv" as "#Hinv". (* What rule is applied here? *) *)
(* □P gets moved to the right context *)
  wp_apply wp_fork.
  - iInv rndN as (b) "Hb".
  	wp_store. eauto with iFrame.
  - wp_pures.
  	iInv rndN as (b') "Hb'".
  	wp_load.
  	iSplitL "Hb'"; first by eauto.
    by iApply "HΦ".
Qed.

Print Scope expr_scope.

(* Possible to pass x, y inside the expr ? *)
Definition swap (x y : loc) : val :=
	λ: <>, let: "z" := !#x in
		#x <- !#y ;; #y <- "z".

(* Yes *)
Definition swap2 : val :=
	λ: "x" "y", let: "z" := !"x" in
		"x" <- !"y" ;; "y" <- "z".

Lemma swap2_spec ℓ₁ ℓ₂ v₁ v₂ :
	{{{ ℓ₁ ↦ v₁ ∗ ℓ₂ ↦ v₂ }}}
		(* App *) swap2 #ℓ₁ #ℓ₂
	{{{ RET #(); ℓ₁ ↦ v₂ ∗ ℓ₂ ↦ v₁ }}}.
Proof.
	iIntros (Ψ) "(H1 & H2) HΨ".
	unfold swap2.
	wp_pures. wp_load. wp_let. wp_load. wp_store.
	(* What happens to the goal in this last step? *)
	(* WP of a val is the same as the new goal *)
	wp_store.
	iApply ("HΨ" with "[$]").
	(* iFrame *)
Qed.

Definition try_ref : val :=
	λ: <>, let: "ℓ" := ref NONEV in
		"ℓ" <- SOMEV #true ;; !"ℓ".

Lemma try_ref_spec :
	{{{ True }}} try_ref #() {{{ RET SOMEV #true; True }}}.
Proof.
	iIntros (Ψ) "_ HΨ"; unfold try_ref; wp_pures.
	wp_alloc l. wp_let. wp_store. wp_load. by iApply "HΨ".
Qed.

Definition try_par (ℓ : loc) : expr :=
	((#ℓ <- !#ℓ + #1) ||| (#ℓ <- !#ℓ + #1)) ;; !#ℓ.

Lemma try_par_spec `{spawnG Σ} ℓ (n : Z) :
	{{{ ℓ ↦ #n }}}
		try_par ℓ
	{{{v, RET #v; ⌜(n <= v)%Z⌝ }}}.
Proof.
	iIntros (Ψ) "Hℓ HΨ"; unfold try_par; wp_pures.
	pose proof (nroot .@ "rnd") as rndN.
	iPoseProof
  	(inv_alloc rndN _ (∃ m : Z, ⌜(n <= m)%Z⌝ ∧ ℓ ↦ #m)%I with "[Hℓ]")
  	as "Hinv"; first by eauto.
	iMod "Hinv" as "#Hinv".
	iAssert (WP #ℓ <- ! #ℓ + #1 {{ _, True }})%I as "HH".
	
	{ wp_bind (!_)%E.
		iInv rndN as (m) "[>%Hnm Hℓm]".
		wp_load. iModIntro. iSplitL; first by eauto.
		wp_pures.
		iInv rndN as (k) "[_ Hℓk]".
		wp_store. iSplitL; last by done.
		do 2 iModIntro. iExists (m+1)%Z. iFrame. iPureIntro; lia.
	}
	wp_apply (wp_par (λ _, True%I) (λ _, True%I)); try done.
	iIntros (v1 v2) "[_ _]". iModIntro. wp_pures.
	iInv rndN as (m) "[#>Hnm Hℓm]". wp_load.
	iSplitR "HΨ"; first by eauto. iApply ("HΨ" with "Hnm").
Qed.

Definition p1 : val :=
	λ: "h", let: "ℓ" := ref NONEV in
		(
			if: "h"
			then Skip ;; "ℓ" <- SOMEV #true
			else "ℓ" <- SOMEV #true ;; Skip
		) ;;
		!"ℓ".

Lemma p1_spec (h : bool) :
	{{{ True }}} p1 #h {{{ RET SOMEV #true; True }}}.
Proof.
	iIntros (Ψ) "_ HΨ"; unfold p1; wp_pures.
	wp_alloc l; wp_let.
	
	destruct h; wp_pures; wp_store;
		wp_pures; wp_load; by iApply "HΨ".
Qed.

Definition p2 : val :=
	λ: "h", let: "ℓ" := ref NONEV in
		(
			(if: "h"
			 then Skip ;; "ℓ" <- SOMEV #true
			 else "ℓ" <- SOMEV #true ;; Skip)
		|||
			("ℓ" <- SOMEV #false)
		) ;;
		!"ℓ".

Lemma p2_spec (h : bool) :
	{{{ True }}} p2 #h {{{ b, RET SOMEV #b; True }}}.
Proof.
	iIntros (Ψ) "_ HΨ"; unfold p2; wp_pures.
	wp_alloc l as "Hl"; wp_let.
	pose proof (nroot .@ "rnd") as rndN.
  iPoseProof
  	(inv_alloc rndN _ (∃ (b : bool), l ↦ SOMEV #b ∨ l ↦ NONEV)%I with "[Hl]")
  	as "Hinv"; first by eauto.
	iMod "Hinv" as "#Hinv".
	wp_pures.
	wp_apply wp_par.
	- destruct h; wp_pures; wp_bind (_ <- _)%E;
			iInv rndN as (b) "[?|?]"; wp_store; iModIntro; iSplitL.
		2,4,6,8: admit.
		all: iModIntro; iExists true; eauto.
	- iInv rndN as (b) "[?|?]"; wp_store; iModIntro; iSplitL.
		2,4: admit.
		all: iModIntro; iExists false; eauto.
	- iIntros (? ?) "[_ _]". iModIntro. wp_pures.
		iInv rndN as (m) "[HSb | HN]"; wp_load;
			iModIntro; iSplitR "HΨ".
		4:{ Abort. (* stuck *)
		(* We need ghost state *)

End learn_heap.

From iris.algebra.lib Require Import excl_auth.
From iris.algebra Require Import numbers.

Check excl_authR ZO.

Class ccounterG Σ := CCounterG { ccounter_inG :> inG Σ (excl_authR ZO) }.
Definition ccounterΣ : gFunctors := #[GFunctor (excl_authR ZO)].
Print gFunctors.
Print gFunctor.

Global Instance subG_ccounterΣ {Σ} : subG ccounterΣ Σ → ccounterG Σ.
Proof. solve_inG. Qed.

Section learn_ghost.

Context `{!heapGS Σ, !ccounterG Σ}. (* I dont know what this means *)
Context `{!spawnG Σ}.
Context (N : namespace).

Notation iProp := (iProp Σ).

Check excl_auth_valid.
Check excl_auth_agree_L.
Check excl_auth_update.

Definition count1 : val :=
	λ: <>, let: "l" := ref #0 in
	((FAA "l" #1) ||| (FAA "l" #1)) ;;
	!"l".

Notation "g ↪● v" := (own g (●E v%Z)) (at level 50).
Notation "g ↪◯ v" := (own g (◯E v%Z)) (at level 50).

Lemma count1_spec :
	{{{ True }}} count1 #() {{{ RET #2; True }}}.
Proof using N ccounterG0 heapGS0 spawnG0 Σ. (* ??? *)
	iIntros (Ψ) "_ HΨ"; unfold count1; wp_pures.
	wp_alloc l as "Hl"; wp_let.
	iMod (own_alloc (●E 0%Z ⋅ ◯E 0%Z)) as (γ1) "[Hγ1 Hγ1']";
		first apply excl_auth_valid.
	iMod (own_alloc (●E 0%Z ⋅ ◯E 0%Z)) as (γ2) "[Hγ2 Hγ2']";
		first apply excl_auth_valid.
	iMod (inv_alloc N _ (
		∃ (n1 n2 : Z), l ↦ #(n1 + n2)%Z ∗ γ1 ↪● n1 ∗ γ2 ↪● n2
	)%I with "[Hl Hγ1 Hγ2]") as "#Hinv".
	{ iModIntro; iExists 0%Z, 0%Z; iFrame. }
	wp_pures. (* Could've also done wp_smart_apply *)
	wp_apply (wp_par (λ _, γ1 ↪◯ 1) (λ _, γ2 ↪◯ 1) with "[Hγ1'] [Hγ2']").
	- iInv "Hinv" as "> (%n1 & %n2 & Hl & H1 & H2)".
		iDestruct (own_valid_2 with "Hγ1' H1") as % ->%excl_auth_agree_L.
		wp_faa.
		iMod (own_update_2  with "H1 Hγ1'") as "[HH1 HH2]".
		{ eapply excl_auth_update with (a':=1%Z). }
		(* Non sequitur? Makes more sense later but need it here. Indeed *)
		iModIntro; iFrame; iModIntro.
		iExists 1%Z, n2.
		iSplitL "Hl";
			first	by replace (1+n2)%Z with (0+n2+1)%Z by lia.
			(* quicker way? not really *)
		iFrame. (* We only need HH here *)
	(* The same proof, except 1 -> 2 *)
	- iInv "Hinv" as "> (%n1 & %n2 & Hl & H1 & H2)".
		iDestruct (own_valid_2 with "Hγ2' H2") as %->%excl_auth_agree_L.
		wp_faa.
		iMod
			(own_update_2 _ _ _ _	(excl_auth_update _ _ 1%Z) with "H2 Hγ2'")
			as "HH".
		iDestruct (own_op with "HH") as "[HH1 HH2]".
		iModIntro; iFrame; iModIntro.
		iExists n1, 1%Z.
		iSplitL "Hl";
			first replace (n1+1)%Z with (n1+0+1)%Z; eauto with lia.
		iFrame.
	- iIntros "* [HH1 HH2] !>".
		wp_seq.
		iInv "Hinv" as "> (%n1 & %n2 & Hl & H1 & H2)".
		wp_load.
		iDestruct (own_valid_2 with "H1 HH1") as %->%excl_auth_agree_L.
		iDestruct (own_valid_2 with "H2 HH2") as %->%excl_auth_agree_L.
		iModIntro.
		iSplitR "HΨ"; last by iApply "HΨ".
		eauto with iFrame.
Qed.

End learn_ghost.

About count1_spec.

Definition inc_example : val :=
  rec: "inc" "hd" :=
    match: "hd" with
      NONE => #()
    | SOME "l" =>
       let: "tmp1" := Fst !"l" in
       let: "tmp2" := Snd !"l" in
       "l" <- (("tmp1" + #1), "tmp2");;
       "inc" "tmp2"
    end.

Definition heap_fact : val :=
	rec: "fac" "n" :=
		if: "n" = #0
		then #1
		else ("n" * ("fac" ("n" - #1))).

Print heap_fact.

Lemma factorial_spec n :
	{{{ ⌜n ≥ 0⌝ }}}
		heap_fact #n
	{{{ v, RET #v; ⌜v = fact n⌝ }}}.
Proof.
	iStartProof.
	iIntros (Ψ) "nge0 H".
	iLöb as "IH".
	wp_rec.
	
	induction n.
	- wp_pures.
		Timeout 1 iApply ("H" #1).



End learn_heap.


