From iris.base_logic Require Import invariants.
From iris_ni.logrel Require Import types.
From iris_ni.program_logic Require Import dwp heap_lang_lifting.
From iris.proofmode Require Import tactics.
From iris_ni.logrel Require Import interp.
From iris_ni.proofmode Require Import dwp_tactics.
From iris.heap_lang Require Import lang proofmode.

Definition rand : val := λ: <>,
  let: "x" := ref #true in
  Fork ("x" <- #false);;
  !"x".



Section proof.
  Context `{!heapDG Σ}.

  (* In Coq, we do not use the AWP proposition for atomic weakest
     preconditions we used in the paper. Rather, in the rule DWP-AWP
     (in the formalization: dwp_atomic_lift_wp) we require the
     expressions e1 and e2 to be atomic and produce no forked off
     threads. Then, we fall back onto the total weakest precondition
     from Iris. This allows us to reuse a lot of lemmas and tactics
     about total weakest preconditions from Iris. *)

  Lemma rand_sec ξ :
    ⊢ DWP rand #() & rand #() : ⟦ tbool Low ⟧ ξ.
  Proof.

    unlock rand.
    (* We first use DWP-PURE to symbolically execute a beta-reduction *)
    dwp_pures.
    (* We then use DWP-BIND to focus on the ref(true) subexpression *)
    dwp_bind (ref #true)%E (ref #true)%E.
    
    (* We then symbolically execute the allocation, using DWP-AWP and
       AWP-ALLOC, obtaining l1 ->l true and l2 ->r true *)
    pose (Φ1 := (λ v, ∃ (l : loc), ⌜v = #l⌝ ∗ l ↦ₗ #true)%I).
    pose (Φ2 := (λ v, ∃ (l : loc), ⌜v = #l⌝ ∗ l ↦ᵣ #true)%I).
    iApply (dwp_atomic_lift_wp Φ1 Φ2).
    { rewrite /TWP1 /Φ1. wp_alloc l1 as "Hl". eauto with iFrame. }
    { rewrite /TWP2 /Φ2. wp_alloc l1 as "Hl". eauto with iFrame. }
    iIntros (? ?).
    iDestruct 1 as (l1 ->) "Hl1".
    iDestruct 1 as (l2 ->) "Hl2". clear Φ1 Φ2.
    iNext.
    dwp_pures=>/=.

    (* It is tempting to use DWP-FORK; but in both threads we need
       l1 ->l _ and l2 ->r _ to symbolically execute the dereference
       and assignment. To share the points-to connectives, we put them
       into an Iris-style invariant.
       We can use DPW-INV-ALLOC to allocate the invariant Hinv. This
       invariant not only allows different threads to access l1 and l2
       (via INV-DUP) but it also ensure that they contain the same
       value throughout the execution. *)
    pose (N := nroot.@"rand").
    iMod (inv_alloc N _
             (∃ (b : bool), l1 ↦ₗ #b ∗ l2 ↦ᵣ #b)%I
            with "[-]") as "#Hinv".
    { eauto with iFrame. }
    
    (* We apply DWP-FORK and get two new goals *)
    dwp_bind (Fork _) (Fork _).
    iApply (dwp_wand with "[-]").
    { (* 1)
         The first goal involves proving that the assignment of false
         to l1 and l2 is secure. We verify this via DWP-INV, and
         temporarily opening the invariant Hinv to obtain l1 ->l b and
         l2 ->r b.*)
      iApply (logrel_fork ξ).
      pose (Φ1 := (λ v, ⌜v = #()⌝ ∗ l1 ↦ₗ #false)%I).
      pose (Φ2 := (λ v, ⌜v = #()⌝ ∗ l2 ↦ᵣ #false)%I).
      iApply dwp_atomic.
      iInv N as (b) "[>Hl1 >Hl2]" "Hcl". iModIntro.
      (* We then apply DWP-AWP and symbolically execute the assigment 
         to obtain l1 ->l false and l2 ->r false. *)
      iApply (dwp_atomic_lift_wp Φ1 Φ2 with "[Hl1] [Hl2] [-]"); try done.
      - rewrite /TWP1 /Φ1. wp_store. eauto.
      - rewrite /TWP2 /Φ2. wp_store. eauto.
      - (* At the end of this atomic step we verify that the invariant
           still holds. *)
        iIntros (? ?) "[-> Hl1] [-> Hl2]". iNext.
        iMod ("Hcl" with "[-]") as "_".
        + iNext. iExists false. iFrame.
        + iModIntro. eauto. }
    (* 2) 
       The second goal is solved in a similar way. When we dereference
       l1 and l2 we know that they contain the same value because of
       the invariant Hinv *)
    iIntros (? ?) "[-> ->]". dwp_pures=>/=.
    iApply dwp_atomic; try done.
    iInv N as (b) "[>Hl1 >Hl2]" "Hcl".
    iModIntro. iApply (dwp_load with "Hl1 Hl2").
    iIntros "Hl1 Hl2". iNext.
    iMod ("Hcl" with "[-]") as "_".
    { iNext. eauto with iFrame. }
    iModIntro. iExists b,b. eauto.
  Qed.

  Lemma rand_sec_typing ξ :
    ⊢ DWP rand #() & rand #() : ⟦ tbool Low ⟧ ξ.
  Proof.
    unlock rand.
    dwp_pures. simpl.
    change (tbool Low) with (stamp (tbool Low) Low).
    iApply (logrel_app with "[] []").
    - dwp_pures. iApply (logrel_lam _ _ _ _ (tref (tbool Low)) with "[]").
      iModIntro. iIntros (v1 v2) "#Hvv". simpl.
      iApply logrel_seq.
      + iApply logrel_fork. iApply logrel_store; eauto.
        * iApply (dwp_value with "Hvv").
        * iApply dwp_value. iModIntro. iExists false,false.
          eauto.
      + iApply logrel_deref. rewrite left_id.
        iApply (dwp_value with "Hvv").
    - iApply logrel_alloc. iApply dwp_value.
      iModIntro. iExists true,true. eauto.
  Qed.
End proof.
